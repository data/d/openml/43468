# OpenML dataset: MMA-Fight-Predictions-by-Professional-Fighters

https://www.openml.org/d/43468

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This is a continually updated dataset of professional fighters making fight predictions.

Content
The data is gathered mostly from James Lynch's YouTube channel, where fighters are asked to predict upcoming fights. Currently, the dataset includes 2,552 made predictions of 244 fights since the end of 2015 up to today.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43468) of an [OpenML dataset](https://www.openml.org/d/43468). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43468/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43468/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43468/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

